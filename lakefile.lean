import Lake
open Lake DSL

package eff {
  -- add package configuration options here
}

@[default_target]
lean_lib eff {
  roots := #[
    `Data.List.Elem,
    `Data.List.Union,
    `Data.List.Subset,

    `Control.Eff,
    `Control.Free
  ]
}

lean_exe test_handle {
  root := `Example.Handle
}
lean_exe test_control {
  root := `Example.HandleControl
}
