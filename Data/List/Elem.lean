/-- Proof that a list has an element -/
class inductive Elem : α -> List α -> Type _ where
| zero : Elem x (x::xs)
| succ : Elem x xs -> Elem x (y::xs)

namespace Elem
  instance : Elem a (a :: xs) := Elem.zero
  instance [prev: Elem a xs] : Elem a (x::xs) := Elem.succ prev

  example : Elem 4 [4] := Elem.zero
  
  def toNat : Elem x xs -> Nat
  | .zero => Nat.zero
  | .succ next => Nat.succ (toNat next)

  instance : SizeOf (Elem x xs) where
    sizeOf := toNat

end Elem
