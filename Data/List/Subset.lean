prelude
import Data.List.Elem

class inductive Subset : List α -> List α -> Type _ where
| nil : Subset [] anylist
| cons : Elem x ys -> Subset xs ys -> Subset (x::xs) ys

namespace Subset
  -- @[reducible]
  def lemma_subset : Subset xs ys -> Elem x xs -> Elem x ys
  | .cons ey _, .zero => ey
  | .cons _ subset', .succ ex' => lemma_subset subset' ex'

  instance : SizeOf (Subset xs ys) where
    sizeOf _ := xs.length

end Subset
