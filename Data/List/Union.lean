prelude
import Data.List.Elem
import Data.List.Subset

/-- Dependent Sum type
    useful for being sum type of list of monad -/
inductive Union : {α : Type u} -> List (α -> Sort v) -> α -> Sort _ where
| mk [prf: Elem ε εs] : (x : ε α) -> Union εs α

namespace Union
  example : Type _ := Union [IO] Unit
  
  def simplify : Union [ε] α -> ε α
  | Union.mk (prf := Elem.zero) x => x

  def weaken1 : Union xs α -> Union (x::xs) α
  | Union.mk (prf := pos) x => mk (prf := Elem.succ pos) x

  def weaken [prf : Subset xs ys] : Union xs α -> Union ys α
  | Union.mk (prf := prf_elem) x => Union.mk (prf := Subset.lemma_subset prf prf_elem) x

end Union
