prelude
import Data.List.Elem
import Data.List.Union
import Data.List.Subset
import Control.Free

/-- Algebraic Effect -/
@[reducible]
def Eff es := Free (Union es)

namespace Eff
  def lift1 [has: Elem ε εs] (simple: ε α) : Eff εs α :=
    let un : Union εs α := Union.mk simple
    Free.inject un

  -- TODO: this is buggy
  -- instance [has: Elem ε εs] : Coe (ε α) (Eff εs α) where
  --   coe := lift1

  /-- Flatten computation with only 1 effect, given that effect can be flattened -/
  def run1 [Pure ε] [Bind ε] : (stacked: Eff [ε] α) -> ε α
  | Free.pure x => pure x
  | Free.bind val cont => bind (Union.simplify val) (λ a => run1 (cont a))
  decreasing_by sorry
  -- TODO: prove this
  -- stacked is getting structurally smaller

  -- instance [Pure ε] [Bind ε] : Coe (Eff [ε] α) (ε α) where
  --   coe := run1

  -- instance : Coe (Eff [ε] α) (Free ε α) where
  --   coe mval := Functor.map Union.simplify mval
  
  def weaken1 : Eff es α -> Eff (e :: es) α
  | .pure x => pure x
  | .bind val cont => Free.bind (Union.weaken1 val) (weaken1 ∘ cont)
  decreasing_by sorry
  
  example (callback : Unit -> Eff es₁ β) : Eff (IO :: es₁) β := do
    lift1 (IO.println "Hello")
    weaken1 $ callback ()

  def handle1 (handler : {α: Type u} -> ε α -> Eff εs α) : Eff (ε :: εs) β -> Eff εs β
  | .pure x => pure x
  | .bind val cont =>
    let cont' := ((handle1 handler) ∘ cont)
    match val with
    | Union.mk (prf := prf) mval => match h:prf with
      | Elem.zero =>
        let mval' := (handler mval)
        bind mval' cont'
      | Elem.succ x => .bind (Union.mk (prf := x) mval) cont'
  decreasing_by sorry

  def transmute [prf : Subset xs ys] : Eff xs α -> Eff ys α
  | .pure x => pure x
  | .bind val cont => do
    let val' : Union ys _ := Union.weaken (prf := prf) val
    let val'' <- Free.inject val'
    transmute (cont val'')
  decreasing_by sorry

  def handle1_auto [prf : Subset ys (ε :: εs)] (handler : {α: Type u} -> ε α -> Eff εs α) (val : Eff ys β) : Eff εs β :=
    let val' : Eff (ε :: εs) β := transmute val
    handle1 handler val'

  -- def handle_all : Handler ε εs -> Eff εs α -> ε α

end Eff

inductive Handler : (_monad: Type u -> Sort v) -> List (Type u → Sort v) → Type _ where
| nil : Handler m []
| cons : (handler : {α: Type u} -> m α -> Eff εs α) -> Handler m εs -> Handler m (ε::εs)

-- (handler : {α: Type u} -> ε α -> Eff εs α)
