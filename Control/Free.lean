
/-- Turn any type into a monad by treating term of that type as action 
 -- See example below in this file for usage -/
inductive Free : (_monadish: Type u -> Type v) -> (_ret: Type u) -> Type _ where
| pure : α -> Free ε α
| bind : ε α -> (α -> Free ε β) -> Free ε β

--  WIP: Free with depth
-- inductive Free : {depth : Nat}  -> (_monadish: Type u -> Type v) -> (_ret: Type u) -> Type _ where
-- | pure : α -> Free (depth := 0) ε α
-- | bind : ε α -> (α -> Free (depth := x) ε β) -> Free (depth := x.succ) ε β

namespace Free
  instance : Pure (Free ε) where 
    pure := Free.pure

  def bindrecur (val: Free ε α) (cont: α -> Free ε β) : Free ε β :=
    match val with
    | pure v => cont v
    | bind mval mcont => bind mval (λ a' => bindrecur (mcont a') cont)

  instance : Bind (Free ε) where
    bind := bindrecur

  def map (fmap : α -> β) (val: Free ε α) : Free ε β := bindrecur val (Free.pure ∘ fmap)

  instance : Monad (Free ε) where
    map := map

  def inject (mval: ε α) : Free ε α := bind mval pure


  /- exmaple usage of Free monad -/
  private
  inductive WhatDogsDo : Type -> Type _ where
  | bark  : (how_loud: Nat) -> WhatDogsDo Unit
  | sleep : (how_long: Nat) -> WhatDogsDo Unit
  | is_sleepy : WhatDogsDo Bool
  example : Free WhatDogsDo Unit := do
    let sleepy <- inject $ WhatDogsDo.is_sleepy
    if sleepy then
      inject $ WhatDogsDo.sleep 20
    else
      inject $ WhatDogsDo.bark 10
end Free
