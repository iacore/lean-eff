inductive SubGraph : Type Tα -> Effectset -> Type Tβ -> Type _ where
| pure : (α -> β) -> SubGraph α Effectset.empty β
| parallel : SubGraph α ε β -> SubGraph γ ζ δ -> SubGraph (α, γ) (Effectset.parallel ε ζ) (β, δ)
| sequential : SubGraph α ε β -> SubGraph γ ζ δ -> SubGraph α ε δ

-- def forkme : (n: Nat) -> Eff [Parallel IO (pow n 2)] Unit
-- | 0 => pure ()
-- | sx@(Nat.succ x) => do
--   IO.println sx
--   let ((), ()) <- Eff.simul (forkme x) (forkme x)
--   pure ()

-- def main : IO () := Eff.runAsync $ forkme 3
