prelude
import Control.Eff

inductive BeepBoop : Type -> Type _ where
| beep : BeepBoop Unit
| boop : BeepBoop Unit


def mixed : Eff [BeepBoop, IO] Unit := do
  Eff.lift1 $ IO.println "Hello"
  Eff.lift1 $ BeepBoop.beep
  Eff.lift1 $ IO.println "World"


def handle_BeepBoop_IO : BeepBoop α -> Eff [IO] α
| .beep => IO.println "beep"
| .boop => IO.println "boop"

def only_IO : Eff [IO] Unit := Eff.handle1 handle_BeepBoop_IO mixed

def main : IO Unit := Eff.run1 only_IO
